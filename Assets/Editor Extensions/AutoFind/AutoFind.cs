﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFind : PropertyAttribute {
	public bool searchInChildren;

	/// <summary>
	/// Add a Find button in the inspector.
	/// </summary>
	/// <param name="ObjectType">Type of the component.</param>
	/// <param name="SearchInChildren">If set to <c>true</c>, will search in children.</param>
	public AutoFind(bool SearchInChildren = false){
		this.searchInChildren = SearchInChildren;
	}
}
