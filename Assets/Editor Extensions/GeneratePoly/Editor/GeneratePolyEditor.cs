using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (GeneratePoly))]
public class GeneratePolyEditor : Editor {
	 public override void OnInspectorGUI () {
        DrawDefaultInspector();
        
        GeneratePoly generateRockScript = (GeneratePoly)target;
        if(GUILayout.Button("Generate"))
            generateRockScript.Generate();
    }
}