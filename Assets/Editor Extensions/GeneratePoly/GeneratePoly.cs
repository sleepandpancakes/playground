using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (MeshRenderer))]
[RequireComponent(typeof (MeshFilter))]
[ExecuteInEditMode]
public class GeneratePoly : MonoBehaviour {

	public int sides = 8;
	public float size = 2f;
	public Material material;

	private MeshFilter mf;
	private MeshRenderer mr;

	void Reset () {
		mf = GetComponent<MeshFilter>();
		mr = GetComponent<MeshRenderer>();
		Generate();
	}

	public void Generate () {
		mf.mesh = GenerateMesh(GenerateVertices().ToArray());
	}

	List<Vector2> GenerateVertices () {
		List<Vector2> vertices = new List<Vector2>();

		float radBySide = 360/sides;

		for (int angle = 0; angle < 360;) {
			vertices.Add(new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad)) * size);
			
			angle += (int)(0.75f * radBySide + (0.40f * radBySide *UnityEngine.Random.value));
		}

		return vertices;
	}

	Vector2[] GenerateUVs (Vector2[] vertices) {
		Vector2[] uvs = new Vector2[vertices.Length];

		for(int x = 0; x < vertices.Length; x++)
		{
			if((x%2) == 0)
				uvs[x] = new Vector2(0,0);
			else
				uvs[x] = new Vector2(1,1);
    	}

		return uvs;
	}

	int[] GenerateTriangles (Vector2[] vertices) {
		if(vertices.Length < 3)
			throw new Exception("Cannot triangulate less than 3 vertices");
		
		int[] triangles = new int[3 * (vertices.Length - 2)];    //3 verts per triangle * num triangles
		
		int C1 = 0;
		int C2 = 1;
		int C3 = 2;
		
		for(int x = 0; x < triangles.Length; x+=3)
		{
			triangles[x] = C1;
			triangles[x+1] = C2;
			triangles[x+2] = C3;
		
			C2++;
			C3++;
		}

		return triangles;
	}

	Mesh GenerateMesh (Vector2[] vertices) {
		Mesh newMesh = new Mesh();
		
		newMesh.vertices = Array.ConvertAll(vertices, vec2 => (Vector3)vec2);
		newMesh.uv = GenerateUVs(vertices);
		newMesh.triangles = GenerateTriangles(vertices);

		//Recalculations
		newMesh.RecalculateNormals();
		newMesh.RecalculateBounds();

		newMesh.name = "Sprite Mesh";
		
		mr.material = material;
		return newMesh;
	}
}
