﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class ScriptedQuote : Quote {

	public TextCollection script;

	protected override void Awake ()
	{
		base.Awake ();
		ClearGooglies.ResetEvent += ResetScript;
	}

	public override void ShowText (float duration)
	{
		text.text = script.Next();
		base.ShowText (duration);
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		ClearGooglies.ResetEvent -= ResetScript;
	}

	void ResetScript(bool quit)
	{
		script.Reset();
		ClearText();
	}

}
