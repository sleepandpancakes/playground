﻿using UnityEngine;
using System.Collections;

public static class AudioExtensionMethods {

	//EastShade Studios tweet:
	public static float ToAudioLevel(this float x)
	{
		if (x==0)
			return -80f;

		return Mathf.Lerp(-30, 0, x);
	}

	/// <summary>
	/// Fades out AudioSource volume to 0 without stopping playback.
	/// </summary>
	public static IEnumerator FadeOut(this AudioSource source, float duration)
	{
		return LerpVolumeTo(source, 0f, duration);
	}

	public static IEnumerator FadeOutAndStop(this AudioSource source, float duration)
	{
		float currentTime = 0;
		float startVolume = source.volume;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;

			source.volume = Mathf.Lerp(startVolume, 0f, lerpTime);

			currentTime += Time.deltaTime;
			yield return null;
		}

		source.volume = 0f;
		source.Stop();
	}

	/// <summary>
	/// Starts playback of audio by fading in
	/// </summary>
	public static IEnumerator FadeInTo(this AudioSource source, float endVolume, float duration)
	{
		source.volume = 0f;
		source.Play();
		return source.LerpVolumeTo(endVolume, duration);
	}

	public static IEnumerator LerpVolumeTo(this AudioSource source, float volume, float duration)
	{
		float currentTime = 0;
		float startVolume = source.volume;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;

			source.volume = Mathf.Lerp(startVolume, volume, lerpTime);

			currentTime += Time.deltaTime;
			yield return null;
		}

		source.volume = volume;
	}

	public static float CorrectedTime(this AudioSource source)
	{
		int sampleFrequency = source.clip.frequency;
		return source.timeSamples / sampleFrequency;
	}



}
