﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public static class ExtensionMethods {

	public static GameObject MakeChildGameObject(this GameObject obj)
	{
		return obj.MakeChildGameObject(Vector3.zero);
	}

	public static GameObject MakeChildGameObject(this GameObject obj, Vector3 localPos)
	{
		GameObject newObj = new GameObject();
		newObj.transform.parent = obj.transform;
		newObj.transform.localPosition = localPos;
		newObj.transform.localScale = Vector3.one;
		newObj.transform.localRotation = Quaternion.identity;
		return newObj;
	}

	public static GameObject MakeChildGameObject(this GameObject obj, string name)
	{
		GameObject newObj = obj.MakeChildGameObject(Vector3.zero);
		newObj.name = name;
		return newObj;
	}

	public static GameObject MakeChildGameObject(this GameObject obj, string name, Vector3 localPos)
	{
		GameObject newObj = obj.MakeChildGameObject(localPos);
		newObj.name = name;
		return newObj;
	}

	//Transform---------------------------------------------------------------------------------
	#region

	public static IEnumerator LerpRectPositionTo(this RectTransform rect, Vector2 end, float duration, AnimationCurve curve)
	{
		return LerpRectPosition(rect, rect.anchoredPosition, end, duration, curve);
	}

	public static IEnumerator LerpRectPosition(this RectTransform rect, Vector2 start, Vector2 end, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			rect.anchoredPosition = Vector2.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		rect.anchoredPosition = end;
	}

//	public static Vector2 CameraCanvasUIPositionOfWorldVector(this RectTransform targetRect, Vector3 worldPosition, Camera cam)
//	{
//		Vector2 result;
//	 	Vector3 screenPosUnadjusted = Camera.main.WorldToScreenPoint(worldPosition);
//	 	RectTransform parentRect = (RectTransform)targetRect.parent;
//		RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, screenPosUnadjusted, cam, out result);
//		return result;
//	}

	public static IEnumerator ShrinkToZero(this Transform t, float duration, AnimationCurve curve)
	{
		return LerpScale(t, t.localScale.x, 0f, duration, curve);
	}

	//standard scaling
	public static IEnumerator LerpScaleTo(this Transform t, Vector3 end, float duration)
	{
		return LerpScale(t, t.localScale, end, duration);
	}

	public static IEnumerator LerpScale(this Transform t, Vector3 start, Vector3 end, float duration)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			t.localScale = Vector3.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localScale = end;
	}

	//scaling using curve
	public static IEnumerator LerpScaleToCurve(this Transform t, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;

			t.localScale = curve.Evaluate(lerpTime) * Vector3.one;

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localScale = curve.Evaluate(1f) * Vector3.one;
	}

	//using vector3
	public static IEnumerator LerpScaleTo(this Transform t, Vector3 end, float duration, AnimationCurve curve)
	{
		return LerpScale(t, t.localScale, end, duration, curve);
	}

	public static IEnumerator LerpScale(this Transform t, Vector3 start, Vector3 end, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			t.localScale = Vector3.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localScale = end;
	}


	/// <summary>
	/// For curves that start at 1
	/// </summary>
	public static IEnumerator LerpScaleLiteral(this Transform t, Vector3 scale, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			t.localScale = scale * lerpFraction;

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localScale = scale;
	}

	//using float
	public static IEnumerator LerpScaleTo(this Transform t, float end, float duration, AnimationCurve curve)
	{
		return LerpScale(t, t.localScale.x, end, duration, curve);
	}

	public static IEnumerator LerpScale(this Transform t, float start, float end, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			t.localScale = Vector3.one * Mathf.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localScale = Vector3.one * end;
	}

	public static IEnumerator LerpLocalPosition(this Transform t, Vector3 start, Vector3 end, float duration, AnimationCurve curve)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			t.localPosition = Vector3.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		t.localPosition = end;
	}

	public static IEnumerator LerpLocalPositionTo(this Transform t, Vector3 end, float duration, AnimationCurve curve)
	{
		Vector3 start = t.localPosition;
		return LerpLocalPosition(t, start, end, duration, curve);
	}

	#endregion

	//Renderer---------------------------------------------------------------------------------
	#region
	public static IEnumerator LerpMaterial(this Renderer rend, Material start, Material end, float duration)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			rend.material.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		rend.sharedMaterial = end;
	}

	public static IEnumerator LerpMaterial(this Renderer rend, Material end, float duration)
	{
		Material start = rend.material;
		return LerpMaterial(rend, start, end, duration);
	}

	public static IEnumerator LerpColor(this SpriteRenderer rend, Color start, Color end, float duration)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			rend.color = Color.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		rend.color = end;
	}

//	public static IEnumerator LerpColor(this Renderer rend, Color start, Color end, float duration)
//	{
//		float currentTime = 0;
//
//		while (currentTime < duration)
//		{
//			float lerpTime = currentTime / duration;
//			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);
//
//			rend.material.color = Color.Lerp(start, end, lerpFraction);
//
//			currentTime += Time.deltaTime;
//			yield return null;
//		}
//
//		rend.material.color = end;
//	}

	public static IEnumerator LerpAlpha(this SpriteRenderer rend, float end, float duration)
	{
		Color target = rend.color;
		target.a = end;
		return LerpColor(rend, rend.color, target, duration);
	}

	public static IEnumerator FadeOut(this SpriteRenderer rend, float duration)
	{
		return LerpAlpha(rend, 0f, duration);
	}

	#endregion

	//Material------------------
	#region 

	public static IEnumerator LerpColor(this Material mat, Color start, Color end, float duration)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			mat.color = Color.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		mat.color = end;
	}

	public static IEnumerator LerpAlpha(this Material mat, float end, float duration)
	{
		Color target = mat.color;
		target.a = end;
		return LerpColor(mat, mat.color, target, duration);
	}

	#endregion

	//UI Canvas Groups---------------------------------------------------------------------------------
	#region
	public static void DisableAndHide(this CanvasGroup panel)
	{
		panel.alpha = 0f;
		panel.blocksRaycasts = false;
//		panel.interactable = false;
	}

	public static void EnableAndShow(this CanvasGroup panel)
	{
		panel.alpha = 1f;
		panel.blocksRaycasts = true;
//		panel.interactable = true;
	}

	public static IEnumerator FadeIn(this CanvasGroup panel, float duration, bool blockRaycast = true)
	{
		return LerpAlpha(panel, panel.alpha, 1f, duration, blockRaycast);
	}

	public static IEnumerator FadeOut(this CanvasGroup panel, float duration, bool blockRaycast = false)
	{
		return LerpAlpha(panel, panel.alpha, 0f, duration, blockRaycast);
	}

	public static IEnumerator LerpAlpha(this CanvasGroup panel, float start, float end, float duration)
	{
		float currentTime = 0;
		panel.blocksRaycasts = false;
		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			panel.alpha = Mathf.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		panel.alpha = end;
		panel.blocksRaycasts = (end == 0f);
	}

	public static IEnumerator LerpAlpha(this CanvasGroup panel, float start, float end, float duration, bool blockRaycast)
	{
		float currentTime = 0;
		panel.blocksRaycasts = false;
		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			panel.alpha = Mathf.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		panel.alpha = end;
		panel.blocksRaycasts = blockRaycast;
	}

	public static IEnumerator LerpAlphaTo(this CanvasGroup panel, float end, float duration)
	{
		return LerpAlpha(panel, panel.alpha, end, duration);
	}

	public static IEnumerator LerpAlphaTo(this CanvasGroup panel, float end, float duration, bool blockRaycast)
	{
		return LerpAlpha(panel, panel.alpha, end, duration, blockRaycast);
	}

	#endregion

	//UI Graphics--------------------------------------------------------------------------------
	#region
	public static IEnumerator LerpColor(this Graphic graphic, Color start, Color end, float duration)
	{
		float currentTime = 0;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = lerpTime * lerpTime * (3f - 2f * lerpTime);

			graphic.color = Color.Lerp(start, end, lerpFraction);

			currentTime += Time.deltaTime;
			yield return null;
		}

		graphic.color = end;
	}

	public static IEnumerator LerpColorTo(this Graphic graphic, Color end, float duration)
	{
		return LerpColor(graphic, graphic.color, end, duration);
	}
	#endregion


	//Camera
	public static float GetOrthographicHeight(this Camera cam)
	{
		return cam.orthographicSize * 2f;
	}

	public static float GetOrthographicWidth(this Camera cam)
	{
		return GetOrthographicHeight(cam) * cam.aspect;
	}


	//Line
	#region
	public static float GetLength(this LineRenderer rend)
	{
		Vector3[] vertices = new Vector3[rend.numPositions];
		rend.GetPositions(vertices);

		if (vertices.Length == 0)
			return 0f;

		float total = 0f;
		for (int i = 1; i < vertices.Length; i++)
			total += Vector3.Distance(vertices[i], vertices[i-1]);

		return total;
	}

	#endregion
}