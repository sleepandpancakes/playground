﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ReplicateZoomCube : Replicate {

	[AutoFind]
	public LightBurst poopLight;
	public CubeHolder cubeHolder;
	public AudioSource radio;
	public float spatialBlend = 0.8f;

	protected override GooglyGrow Clone ()
	{
		GooglyGrow newObj = base.Clone (cubeHolder.cube);

		newObj.GetComponent<ReplicateZoomCube>().ChangeAudioBlend(radio.timeSamples);
		poopLight.Burst();

		return newObj;
	}

	public void ChangeAudioBlend(int samples)
	{
		radio.spatialBlend = spatialBlend;
		radio.timeSamples = samples;
	}

	protected override void ResetGoogly (bool quit)
	{
		base.ResetGoogly (quit);

		if (!googly.destroyOnReset)
			poopLight.TurnOff();
	}

	protected override IEnumerator ResetRoutine ()
	{
		Coroutine radioRoutine = StartCoroutine(radio.FadeOut(1f));
		yield return transform.LerpScaleTo(Vector3.zero, 1f, AnimationCurve.EaseInOut(0,0,1,1));
		yield return radioRoutine;
		Destroy(gameObject);
	}

	#if UNITY_EDITOR

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.T))
			Clone();


	}

	#endif

}
