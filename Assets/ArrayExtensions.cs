﻿using UnityEngine;
using System.Collections;

public static class ArrayExtensions {

	public static T RandomElement<T>(this T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}
}
