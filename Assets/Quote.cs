﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MovementEffects;

public class Quote : MonoBehaviour {

	[AutoFind]
	public TextMeshPro text;
	public bool showOnAwake;

	CoroutineHandle previousRoutine;

	protected virtual void Awake()
	{
		text.enabled = false;
	}

	public virtual void ShowText(float duration)
	{
		if (previousRoutine != null)
			Timing.KillCoroutines(previousRoutine);

		previousRoutine = Timing.RunCoroutine(ShowTextRoutine(duration));
	}

//	public void ShowText(float duration, string textLine)
//	{
//		text.text = textLine;
//		if (previousRoutine != null)
//			Timing.KillCoroutines(previousRoutine);
//		
//		ShowText(duration);
//	}

	IEnumerator<float> ShowTextRoutine(float duration)
	{
//		if (previousRoutine != null)
//			yield return Timing.WaitUntilDone(previousRoutine);

		text.enabled = true;
		yield return Timing.WaitForSeconds(duration);
		text.enabled = false;
	}

	protected void ClearText()
	{
		Timing.KillCoroutines(previousRoutine);
		text.enabled = false;
	}

	protected virtual void OnDestroy()
	{
		Timing.KillCoroutines(previousRoutine);
	}

}
