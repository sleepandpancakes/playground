﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryAudio : MonoBehaviour {

	[AutoFind]
	public AudioSource speaker;
	public float volume = 0.3f;
	public float duration = 0.8f;
	Coroutine handle;

	void OnMouseDown()
	{
		Play();
	}

	public void Play()
	{
		if (handle != null)
			StopCoroutine(handle);
		
		handle = StartCoroutine(PlayRoutine());
	}

	IEnumerator PlayRoutine()
	{
//		Debug.Log("play");
		speaker.volume = volume;
		yield return new WaitForSeconds(duration);
		speaker.volume = 0f;
	}

}
