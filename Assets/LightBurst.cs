﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LightBurst : MonoBehaviour {

	public float maxRange = 6f;
	public float duration = 1f;
	public AnimationCurve curve;

	[AutoFind]
	public Light pointLight;

	public void Burst()
	{
		pointLight.range += 6f;
	}

	public void TurnOff()
	{
		pointLight.range = 0f;
	}

}
