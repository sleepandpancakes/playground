﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TextCollection : ScriptableObject {

	public string[] linesOfText;
	public bool randomize;
	int i;

	void OnEnable()
	{
		Reset();
	}

	public string Next()
	{
		if (randomize)
			return GetRandomLine();

		else
			return GetNextLine();
	}

	public void Reset()
	{
		i = 0;
	}

	string GetNextLine()
	{
		string line = linesOfText[i % linesOfText.Length];
		i++;
		return line;
	}

	string GetRandomLine()
	{
		return linesOfText.RandomElement();
	}

}
