﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class BlackCube : MonoBehaviour {

	public float timeToAppear = 1f;
	bool visible;

	void Awake()
	{
		gameObject.SetActive(false);
		Timing.RunCoroutine(AppearWhenTimerDone());
	}

	IEnumerator<float> AppearWhenTimerDone()
	{
		yield return Timing.WaitForSeconds(timeToAppear);
		Appear();
	}

	[CatchCo.ExposeMethodInEditor]
	public void Appear()
	{
		Debug.Log("appear");
		gameObject.SetActive(true);
	}

	void OnBecameVisible()
	{
		visible = true;
	}

	void OnBecameInvisible()
	{
		if (visible)
		{
			gameObject.SetActive(false);
		}
	}
		
}
