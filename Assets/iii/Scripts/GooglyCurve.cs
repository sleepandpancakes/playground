﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GooglyCurve : ScriptableObject {

	public AnimationCurve curve;

}
