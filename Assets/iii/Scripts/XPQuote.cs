﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Klak.Motion;

public class XPQuote : Quote {

	[AutoFind]
	public BrownianMotion bMotion;
	[AutoFind]
	public ConstantMotion cMotion;
	public Transform pivot;
	bool textShowing;

	protected override void Awake ()
	{
		base.Awake ();
	}

	public override void ShowText (float duration)
	{
		pivot.SetParent(null);
		pivot.position = Vector3.forward * 10f + Random.insideUnitSphere * 10f;

		text.enabled = true;
		bMotion.enabled = true;
		cMotion.enabled = true;

		//Mark text as showing
		textShowing = true;
	}

	void OnBecameInvisible()
	{
		if (!textShowing)
			return;

//		Debug.Log("bye");
		Destroy(pivot.gameObject);
	}

}
