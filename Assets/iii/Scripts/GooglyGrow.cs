﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Klak.Motion;
using DG.Tweening;

public class GooglyGrow : MonoBehaviour {

//	BrownianMotion motion;
	Vector3 initialScale;
	public Vector3 basalScale;
	public float growDuration = 0.3f;
	public float growFactor = 1.3f;
	public bool destroyOnReset;

	[AutoFind]
	public Quote quote;

//	Coroutine growRoutine;
	Tweener growTween;

	void Awake()
	{
//		motion = GetComponent<BrownianMotion>();
		initialScale = transform.localScale;
		basalScale = initialScale;
	}

	void Update()
	{
		
	}

	void OnMouseEnter()
	{
		growTween.Kill();

		growTween = transform.DOScale(growFactor * basalScale, growDuration);
	}

	void OnMouseExit()
	{
		growTween.Kill();
		growTween = transform.DOScale(basalScale, growDuration);
	}

	public void Jiggle(AnimationCurve curve)
	{
		StartCoroutine(transform.LerpScaleToCurve(1f, curve));
	}

	IEnumerator JiggleRoutine(float duration, AnimationCurve curve)
	{
		float currentTime = 0f;

		while (currentTime < duration)
		{
			float lerpTime = currentTime / duration;
			float lerpFraction = curve.Evaluate(lerpTime);

			basalScale = lerpFraction * initialScale;

			lerpTime += Time.deltaTime;
			yield return null;
		}

		basalScale = initialScale;
	}
		
}
