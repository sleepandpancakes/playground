﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using MovementEffects;

public class Credits : MonoBehaviour {

	public float delay = 2f;
	[AutoFind]
	public TextMeshProUGUI textUI;
	public float typingTimeStep = 0.05f;

	void Awake()
	{
		textUI.maxVisibleCharacters = 0;
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log("Hello");
//			Timing.RunCoroutine(CreditsRoutine());
			StartCoroutine(CreditsRoutine());
		}
	}

	AsyncOperation LoadNextScene()
	{
		return SceneManager.LoadSceneAsync(1);
	}

	IEnumerator CreditsRoutine()
	{
		AsyncOperation load = LoadNextScene();
		load.allowSceneActivation = false;

		yield return load.isDone;
		yield return StartCoroutine(SayMeow());
		Debug.Log("done");

		load.allowSceneActivation = true;
	}

	void OnMouseDown()
	{
		Debug.Log("clicked here");
	}

	IEnumerator SayMeow()
	{
		WaitForSeconds wait = new WaitForSeconds(typingTimeStep);

		for (int i = 0; i < textUI.textInfo.characterCount; i++)
		{
			textUI.maxVisibleCharacters = i + 1;
			yield return wait;
		}
	}

}
