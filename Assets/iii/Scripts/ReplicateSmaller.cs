﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplicateSmaller : Replicate {

	public int divisionFactor = 6;

	protected override GooglyGrow Clone ()
	{
		Vector3 newScale = transform.localScale / divisionFactor;

		base.Clone ().basalScale = newScale;
		base.Clone ().basalScale = newScale;

		GooglyGrow newObj = base.Clone ();
		newObj.basalScale = newScale;
		return newObj;
	}

}
