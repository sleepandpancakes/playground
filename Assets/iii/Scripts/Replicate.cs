﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Replicate : MonoBehaviour {

	[AutoFind]
	public GooglyGrow googly;
	public float quoteDuration = 2.5f;
	public bool dontReplicateCube;
	public bool interactable = true;

	protected AudioSource a_Source;
	public AudioClip[] a_Clips;

	public GooglyCurve curveHolder;

	void Awake()
	{
		a_Source = GetComponent<AudioSource>();

		ClearGooglies.ResetEvent += ResetGoogly;
	}

	void OnDestroy()
	{
		ClearGooglies.ResetEvent -= ResetGoogly;
	}

	void OnMouseDown()
	{
		if (interactable)
			Clone();
	}

	protected virtual GooglyGrow Clone()
	{
		return Clone(googly);
	}

	protected virtual GooglyGrow Clone(GooglyGrow cubeToClone)
	{
		GooglyGrow newObj;

		if (!dontReplicateCube)
		{
			newObj = GameObject.Instantiate(googly);
			newObj.transform.position = transform.position + Random.insideUnitSphere * 10f;		
			newObj.destroyOnReset = true;

			newObj.Jiggle(curveHolder.curve);
		}

		else
			newObj = googly;

		newObj.quote.ShowText(quoteDuration);

		a_Source.PlayOneShot(a_Clips.RandomElement());

		return newObj;
	}

	protected virtual void ResetGoogly(bool quit)
	{
		if (googly.destroyOnReset)
			StartCoroutine(ResetRoutine());
	}

	protected virtual IEnumerator ResetRoutine()
	{
		yield return transform.LerpScaleTo(Vector3.zero, 1f, AnimationCurve.EaseInOut(0,0,1,1));
		Destroy(gameObject);
	}

}
