﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CubeHolder : ScriptableObject {

	public GooglyGrow cube;

}
