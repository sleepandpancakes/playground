﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Klak.Motion;
using UnityEngine.UI;

public class Quitter : MonoBehaviour {

	public MeshRenderer background;
	public Material backgroundQuitMaterial;
	public GameObject[] cubes;
	public BrownianMotion tutorialCubeMotion;
//	public Canvas sadTextCanvas;
	public SadText sadText;
	public Replicate tutorialCube;
	public Light sceneLight;
	public Canvas controlsCanvas;
	public float lightDimDuration = 5f;
	public float growDuration = 10f;

	public AudioSource sadMusicSpeaker;

	bool quitting;

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Quit();	
	}

	void Quit()
	{
		if (quitting)
			return;

		quitting = true;
		Debug.Log("quitting");

		background.sharedMaterial = backgroundQuitMaterial;

		ClearGooglies.TriggerReset(true);

		for (int i = 0; i < cubes.Length; i++)
			cubes[i].SetActive(false);

		tutorialCubeMotion.enabled = false;
//		tutorialCube.googly.enabled = false;
		tutorialCube.interactable = false;
		controlsCanvas.enabled = false;

		sadMusicSpeaker.Play();
		StartCoroutine(DimLight());
		StartCoroutine(GrowCube());
		StartCoroutine(QuitRoutine());
	}

	IEnumerator QuitRoutine()
	{
		yield return sadText.ShowSadText();
		yield return new WaitForSeconds(3f);

		Debug.Log("Quit Done");
		Application.Quit();
	}

	float dimVel = 0f;
	IEnumerator DimLight()
	{
//		Debug.Log("dim");
		while (true)
		{
//			sceneLight.intensity -= Time.deltaTime * lightDimSpeed;
			sceneLight.intensity = Mathf.SmoothDamp(sceneLight.intensity, 0f, ref dimVel, lightDimDuration);
			yield return null;
		}
	}

	Vector3 scaleVel = Vector3.zero;
	IEnumerator GrowCube()
	{
		while (true)
		{
			tutorialCube.transform.localScale = Vector3.SmoothDamp(tutorialCube.transform.localScale, 3f * Vector3.one, ref scaleVel, lightDimDuration);
			tutorialCube.googly.basalScale = tutorialCube.transform.localScale;
			yield return null;
		}	
	}

}
