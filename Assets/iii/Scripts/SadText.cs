﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SadText : MonoBehaviour {

	public TextMeshProUGUI sadTextUI;

	[SerializeField]
	float wordTimeStep = 0.3f;
	int wordCount;

	void Awake()
	{
		sadTextUI.maxVisibleCharacters = 0;
		wordCount = sadTextUI.textInfo.wordCount;
	}

	public Coroutine ShowSadText()
	{
		gameObject.SetActive(true);
		return StartCoroutine(SadTextRoutine());
	}

	IEnumerator SadTextRoutine()
	{
		yield return StartCoroutine(Type());
	}

	IEnumerator Type()
	{
		WaitForSeconds wait = new WaitForSeconds(wordTimeStep);
		yield return new WaitForSeconds(1f);

		StartCoroutine(Spread());
		for (int i = 0; i < wordCount; i++)
		{
			int charCount = sadTextUI.textInfo.wordInfo[i].lastCharacterIndex;
			charCount = sadTextUI.textInfo.characterInfo[charCount].index;
			sadTextUI.maxVisibleCharacters = charCount + 1;
			yield return wait;
		}
	}

	IEnumerator Spread()
	{
//		WaitForSeconds wait = new WaitForSeconds(0.3f);

		while (true)
		{
			sadTextUI.characterSpacing += 0.1f;
			yield return null;
		}
	}

}
