﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClearGooglies : MonoBehaviour {

	public delegate void Reset(bool quit);
	public static event Reset ResetEvent;

	float timeThreshold = 1f;

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
			ResetScene();
	}

	void ResetScene()
	{
		if (Time.timeSinceLevelLoad > timeThreshold)
			ResetEvent(false);
//			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public static void TriggerReset(bool quit)
	{
		ResetEvent(quit);
	}
}

