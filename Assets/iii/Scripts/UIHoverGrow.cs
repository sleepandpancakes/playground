﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class UIHoverGrow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public float growDuration = 0.2f;
	public float growConstant = 1.05f;
	Coroutine growCoroutine;

	void Start()
	{
		
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		Grow();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		Shrink();
	}

	public void Grow()
	{	
		if (growCoroutine != null)
			StopCoroutine(growCoroutine);

		growCoroutine = StartCoroutine( transform.LerpScale(1f, growConstant, growDuration, AnimationCurve.EaseInOut(0,0,1,1)) );
	}

	public void Shrink()
	{
		if (growCoroutine != null)
			StopCoroutine(growCoroutine);

		growCoroutine = StartCoroutine( transform.LerpScale(transform.localScale.x, 1f, growDuration, AnimationCurve.EaseInOut(0,0,1,1)) );
	}

	public void ToggleBehavior(bool toggle)
	{
		if (!toggle)
			Shrink();

		this.enabled = toggle;
	}

}
