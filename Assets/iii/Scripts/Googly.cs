﻿//https://twitter.com/DivineOmega/status/833484729609494528

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Googly : MonoBehaviour {

	public float radius = 1.0f;
	public Transform pupil;

	Vector3 pupilPos;
	Vector3 pupilPosOld;
	float interpolation;

	void FixedUpdate()
	{
		Vector3 pupilPosNew = 2 * pupilPos - pupilPosOld + 0.5f * Physics.gravity * Time.deltaTime * Time.deltaTime;
		pupilPosNew = transform.TransformPoint(ClampPupil(pupilPosNew));

		pupilPosOld = pupilPos;
		pupilPos = pupilPosNew;

		interpolation += Time.deltaTime;
	}

	void Update ()
	{
		interpolation = Mathf.Clamp01((Time.deltaTime - interpolation)/Time.fixedDeltaTime);

		Vector3 setPupilPos = ClampPupil(Vector3.Lerp(pupilPosOld, pupilPos, interpolation));
		pupil.localPosition = setPupilPos;

		interpolation = 0f;
	}

	Vector3 ClampPupil(Vector3 worldPos)
	{
		Vector3 localPos = transform.InverseTransformPoint(worldPos);
		localPos.z = -0.07f;

		if (localPos.sqrMagnitude > radius * radius)
			localPos = localPos.normalized * radius;

		return localPos;
	}
}
