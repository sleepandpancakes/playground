﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MovementEffects;

public class ResetUI : MonoBehaviour {

	[AutoFind]
	public TextMeshProUGUI resetText;
	public float textDuration = 0.8f;

	void Awake()
	{
		ClearGooglies.ResetEvent += ShowText;
	}

	void OnDestroy()
	{
		ClearGooglies.ResetEvent -= ShowText;
	}

	void ShowText(bool quit)
	{
		if (!quit)
			Timing.RunCoroutine(ShowTextRoutine());
	}

	IEnumerator<float> ShowTextRoutine()
	{
		resetText.enabled = true;
		yield return Timing.WaitForSeconds(textDuration);
		resetText.enabled = false;
	}

}
